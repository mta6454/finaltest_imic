import 'package:flutter/material.dart';
import 'package:flutter_finaltest/screen/first_screen.dart';
import 'package:flutter_finaltest/screen/mainscreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MainScreen(),
      debugShowCheckedModeBanner: false,
      // home: FirstScreen(),
    );
  }
}
