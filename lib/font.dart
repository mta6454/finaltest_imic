import 'package:flutter/material.dart';

class FontAsset {
  static const String appFont = 'Montserrat';
}

class FontStyle {
  static const TextStyle f3 = TextStyle(
      fontFamily: FontAsset.appFont,
      fontSize: 22,
      color: Color(0xff363E59),
      fontWeight: FontWeight.w900);
  static const TextStyle f2 = TextStyle(
      fontFamily: FontAsset.appFont,
      fontSize: 22,
      color: Color(0xff363E59),
      fontWeight: FontWeight.w500);
  static const TextStyle f1 = TextStyle(
      fontFamily: FontAsset.appFont,
      fontSize: 22,
      color: Color(0xff363E59),
      fontWeight: FontWeight.w400);
  static const TextStyle f4 = TextStyle(
      fontFamily: FontAsset.appFont,
      fontSize: 22,
      color: Color(0xff363E59),
      fontWeight: FontWeight.w700);

  // static const TextStyle headerFont = TextStyle();
}
