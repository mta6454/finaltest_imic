import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_finaltest/font.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProfileScreen extends StatefulWidget {
  ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  bool isActive = false;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children: [
            Container(
              // color: Colors.yellow,
              margin: EdgeInsets.only(top: 25),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        padding: EdgeInsets.only(right: 10),
                        height: 48,
                        width: 48,
                        child: Image.asset('assets/image/Avatar.png'),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Thieu Anh Minh',
                            style: FontStyle.f4.copyWith(
                                color: Color(0xff04004F), fontSize: 15),
                          ),
                          Text(
                            'thieuanhminh1998@gmail.com',
                            style: FontStyle.f1.copyWith(
                                color: Color(0xff04004F), fontSize: 15),
                          )
                        ],
                      ),
                    ],
                  ),
                  Container(
                      child: InkWell(
                        borderRadius: BorderRadius.circular(8),
                        onTap: () {},
                        child: Center(
                          child: Text(
                            'Edit',
                            style: FontStyle.f1
                                .copyWith(fontSize: 12, color: Colors.white),
                          ),
                        ),
                      ),
                      height: 35,
                      width: 83,
                      decoration: BoxDecoration(
                          color: Color(0xff00DDA3),
                          borderRadius: BorderRadius.circular(10))),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    height: 65,
                    width: 105,
                    decoration: BoxDecoration(
                        color: Color(0xffF7F8F8),
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.2),
                              blurRadius: 3,
                              offset: Offset(0, 3))
                        ]),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          '174cm',
                          style: FontStyle.f4
                              .copyWith(color: Color(0xff00DDA3), fontSize: 14),
                        ),
                        Text(
                          'Height',
                          style: FontStyle.f1
                              .copyWith(color: Color(0xff04004F), fontSize: 12),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 65,
                    width: 105,
                    decoration: BoxDecoration(
                        color: Color(0xffF7F8F8),
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.2),
                              blurRadius: 3,
                              offset: Offset(0, 3))
                        ]),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          '135kg',
                          style: FontStyle.f4
                              .copyWith(color: Color(0xff00DDA3), fontSize: 14),
                        ),
                        Text(
                          'Weight',
                          style: FontStyle.f1
                              .copyWith(color: Color(0xff04004F), fontSize: 12),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 65,
                    width: 105,
                    decoration: BoxDecoration(
                        color: Color(0xffF7F8F8),
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.2),
                              blurRadius: 3,
                              offset: Offset(0, 3))
                        ]),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          '24yo',
                          style: FontStyle.f4
                              .copyWith(color: Color(0xff00DDA3), fontSize: 14),
                        ),
                        Text(
                          'Age',
                          style: FontStyle.f1
                              .copyWith(color: Color(0xff04004F), fontSize: 12),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
              height: 159,
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Color(0xffF7F8F8),
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        blurRadius: 3,
                        offset: Offset(0, 3))
                  ]),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Account',
                    style: FontStyle.f2
                        .copyWith(color: Color(0xff04004F), fontSize: 16),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 6),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 10),
                              child: SvgPicture.asset(
                                  'assets/svg/personaldata.svg'),
                            ),
                            Text(
                              'Personal Data',
                              style: FontStyle.f1.copyWith(
                                  color: Color(0xff04004F), fontSize: 12),
                            )
                          ],
                        ),
                        Icon(
                          Icons.arrow_forward_ios_rounded,
                          color: Color(0xff04004F),
                          size: 12,
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 1),
                    padding: EdgeInsets.symmetric(vertical: 6),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 13),
                              child: SvgPicture.asset(
                                  'assets/svg/achievement.svg'),
                            ),
                            Text(
                              'Achievement',
                              style: FontStyle.f1.copyWith(
                                  color: Color(0xff04004F), fontSize: 12),
                            )
                          ],
                        ),
                        Icon(
                          Icons.arrow_forward_ios_rounded,
                          color: Color(0xff04004F),
                          size: 12,
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 6),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 11),
                              child: SvgPicture.asset('assets/svg/history.svg'),
                            ),
                            Text(
                              'Activity History',
                              style: FontStyle.f1.copyWith(
                                  color: Color(0xff04004F), fontSize: 12),
                            )
                          ],
                        ),
                        Icon(
                          Icons.arrow_forward_ios_rounded,
                          color: Color(0xff04004F),
                          size: 12,
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
              margin: EdgeInsets.symmetric(vertical: 15),
              decoration: BoxDecoration(
                  color: Color(0xffF7F8F8),
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        blurRadius: 3,
                        offset: Offset(0, 3))
                  ]),
              width: double.infinity,
              height: 99,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Account',
                    style: FontStyle.f2
                        .copyWith(color: Color(0xff04004F), fontSize: 16),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 1),
                    padding: EdgeInsets.symmetric(vertical: 6),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 13),
                              child: SvgPicture.asset('assets/svg/noti.svg'),
                            ),
                            Text(
                              'Pop-up Notification',
                              style: FontStyle.f1.copyWith(
                                  color: Color(0xff04004F), fontSize: 12),
                            )
                          ],
                        ),
                        AnimatedContainer(
                          duration: Duration(milliseconds: 100),
                          height: 24,
                          width: 44,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: isActive == true
                                  ? Color(0xff00DDA3)
                                  : Color(0xff04004F)),
                          child: InkWell(
                            child: Row(
                              children: [
                                Container(
                                  margin: isActive == false
                                      ? EdgeInsets.only(left: 5)
                                      : EdgeInsets.only(left: 25),
                                  height: 14,
                                  width: 14,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(45)),
                                )
                              ],
                            ),
                            onTap: (() {
                              setState(() {
                                isActive = !isActive;
                              });
                            }),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
              height: 159,
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Color(0xffF7F8F8),
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        blurRadius: 3,
                        offset: Offset(0, 3))
                  ]),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Other',
                    style: FontStyle.f2
                        .copyWith(color: Color(0xff04004F), fontSize: 16),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 6),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 10),
                              child: SvgPicture.asset('assets/svg/mess.svg'),
                            ),
                            Text(
                              'Contact Us',
                              style: FontStyle.f1.copyWith(
                                  color: Color(0xff04004F), fontSize: 12),
                            )
                          ],
                        ),
                        Icon(
                          Icons.arrow_forward_ios_rounded,
                          color: Color(0xff04004F),
                          size: 12,
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 1),
                    padding: EdgeInsets.symmetric(vertical: 6),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 13),
                              child: SvgPicture.asset('assets/svg/policy.svg'),
                            ),
                            Text(
                              'Privacy Policy',
                              style: FontStyle.f1.copyWith(
                                  color: Color(0xff04004F), fontSize: 12),
                            )
                          ],
                        ),
                        Icon(
                          Icons.arrow_forward_ios_rounded,
                          color: Color(0xff04004F),
                          size: 12,
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 6),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 11),
                              child: SvgPicture.asset('assets/svg/setting.svg'),
                            ),
                            Text(
                              'Settings',
                              style: FontStyle.f1.copyWith(
                                  color: Color(0xff04004F), fontSize: 12),
                            )
                          ],
                        ),
                        Icon(
                          Icons.arrow_forward_ios_rounded,
                          color: Color(0xff04004F),
                          size: 12,
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
