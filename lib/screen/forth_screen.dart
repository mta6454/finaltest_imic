import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_finaltest/screen/widget/4th_screen_activitiy.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_finaltest/font.dart';

class FourthScreen extends StatelessWidget {
  FourthScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 15),
              height: 50,
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.15),
                        blurRadius: 3,
                        offset: Offset(0, 3))
                  ],
                  borderRadius: BorderRadius.circular(10),
                  color: Color(0xffF7F8F8)),
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      child: TextField(
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Search...',
                            prefixIcon: Icon(
                              Icons.search,
                              color: Color(0xff04004F),
                            )),
                      ),
                    ),
                  ),
                  Container(
                    height: 20,
                    width: 1,
                    color: Colors.grey,
                  ),
                  Container(
                    child: InkWell(
                        onTap: () {},
                        child: SvgPicture.asset('assets/svg/option.svg')),
                    width: 40,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.horizontal(
                            right: Radius.circular(10))),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 25, bottom: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Activities',
                    style: FontStyle.f4
                        .copyWith(color: Colors.black, fontSize: 17),
                  ),
                  RichText(
                      text: TextSpan(
                          text: 'See more',
                          style: FontStyle.f1.copyWith(
                              color: Color(0xff9B99B9), fontSize: 12)))
                ],
              ),
            ),
            FourthActivities(
              firstText: 'Fullbody Workout',
              secondText: 'Today, 03:00pm',
              image: Container(child: Image.asset('assets/image/workout.png')),
            ),
            SizedBox(
              height: 15,
            ),
            FourthActivities(
              firstText: 'Upperbody Workout',
              secondText: 'June 05, 02:00pm',
              image: Container(
                  child: Image.asset('assets/image/upperworkout.png')),
            ),
            Container(
              margin: EdgeInsets.only(top: 25, bottom: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Nutrition',
                    style: FontStyle.f4
                        .copyWith(color: Colors.black, fontSize: 17),
                  ),
                  RichText(
                      text: TextSpan(
                          text: 'See more',
                          style: FontStyle.f1.copyWith(
                              color: Color(0xff9B99B9), fontSize: 12)))
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  height: 120,
                  width: 164,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.2),
                            blurRadius: 3,
                            offset: Offset(0, 3))
                      ]),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          padding: EdgeInsets.only(bottom: 12),
                          child: Image.asset('assets/image/cake.png')),
                      Text(
                        'Honey Pancake',
                        style: FontStyle.f2
                            .copyWith(color: Colors.black, fontSize: 12),
                      ),
                      Text(
                        'Easy | 30mins | 180kCal',
                        style: FontStyle.f1
                            .copyWith(fontSize: 10, color: Colors.grey),
                      )
                    ],
                  ),
                ),
                Container(
                  height: 120,
                  width: 164,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.2),
                            blurRadius: 3,
                            offset: Offset(0, 3))
                      ]),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          padding: EdgeInsets.only(bottom: 12),
                          child: Image.asset('assets/image/bread.png')),
                      Text(
                        'Canai Bread',
                        style: FontStyle.f2
                            .copyWith(color: Colors.black, fontSize: 12),
                      ),
                      Text(
                        'Easy | 20mins | 230kCal',
                        style: FontStyle.f1
                            .copyWith(fontSize: 10, color: Colors.grey),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 25, bottom: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Mindfulness',
                    style: FontStyle.f4
                        .copyWith(color: Colors.black, fontSize: 17),
                  ),
                  RichText(
                      text: TextSpan(
                          text: 'See more',
                          style: FontStyle.f1.copyWith(
                              color: Color(0xff9B99B9), fontSize: 12)))
                ],
              ),
            ),
            FourthActivities(
              firstText: 'Fullbody Workout',
              secondText: 'Today, 03:00pm',
              image:
                  Container(child: Image.asset('assets/image/2ndactivity.png')),
            ),
            SizedBox(
              height: 15,
            ),
            FourthActivities(
              firstText: 'Fullbody Workout',
              secondText: 'Today, 03:00pm',
              image: Container(child: Image.asset('assets/image/sleep.png')),
            ),
            Container(
              margin: EdgeInsets.only(top: 25, bottom: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'News',
                    style: FontStyle.f4
                        .copyWith(color: Colors.black, fontSize: 17),
                  ),
                  RichText(
                      text: TextSpan(
                          text: 'See more',
                          style: FontStyle.f1.copyWith(
                              color: Color(0xff9B99B9), fontSize: 12)))
                ],
              ),
            ),
            Container(
              height: 85,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        blurRadius: 3,
                        offset: Offset(0, 3))
                  ]),
              child: Row(
                children: [
                  Container(
                    child: Image.asset('assets/image/news.png'),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Your healthy start to the day:',
                          style: FontStyle.f2
                              .copyWith(color: Colors.black, fontSize: 12),
                        ),
                        Text('Do you sometimes lack the inspiration...',
                            style: FontStyle.f1.copyWith(
                                color: Color(0xff9B99B9), fontSize: 10)),
                        Container(
                          margin: EdgeInsets.only(top: 12),
                          child: Text(
                            'Continue reading',
                            style: FontStyle.f4.copyWith(
                                color: Color(0xff0FD8AB), fontSize: 12),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Container(
              height: 85,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        blurRadius: 3,
                        offset: Offset(0, 3))
                  ]),
              child: Row(
                children: [
                  Container(
                    child: Image.asset('assets/image/news.png'),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Your healthy start to the day:',
                          style: FontStyle.f2
                              .copyWith(color: Colors.black, fontSize: 12),
                        ),
                        Text('Do you sometimes lack the inspiration...',
                            style: FontStyle.f1.copyWith(
                                color: Color(0xff9B99B9), fontSize: 10)),
                        Container(
                          margin: EdgeInsets.only(top: 12),
                          child: Text(
                            'Continue reading',
                            style: FontStyle.f4.copyWith(
                                color: Color(0xff0FD8AB), fontSize: 12),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 100,
            )
          ],
        ),
      ),
    ));
  }
}
