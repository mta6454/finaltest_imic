import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_finaltest/font.dart';
import 'package:flutter_finaltest/screen/widget/2nd_screen_activity.dart';

class SecondScreen extends StatelessWidget {
  SecondScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children: [
            Container(
              height: 181,
              decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(10)),
              child: Stack(children: [
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    padding: EdgeInsets.only(left: 12, top: 23),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Understand your',
                          style: FontStyle.f4
                              .copyWith(color: Colors.white, fontSize: 14),
                        ),
                        Text(
                          'Physical well being',
                          style: FontStyle.f1
                              .copyWith(color: Colors.white, fontSize: 13),
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        Container(
                          child: Center(
                            child: Text(
                              "Let's Assess",
                              style: FontStyle.f2
                                  .copyWith(color: Colors.white, fontSize: 10),
                            ),
                          ),
                          height: 35,
                          width: 90,
                          decoration: BoxDecoration(
                              color: Color(0xff00DDA3),
                              borderRadius: BorderRadius.circular(10)),
                        )
                      ],
                    ),
                    height: 143,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Color(0xff130160),
                        borderRadius: BorderRadius.circular(10)),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: Container(
                    child: Image.asset('assets/image/access.png'),
                  ),
                )
              ]),
            ),
            Container(
              margin: EdgeInsets.only(top: 25, bottom: 15),
              alignment: Alignment.centerLeft,
              child: Text(
                'Top Programs',
                style: FontStyle.f4.copyWith(fontSize: 17, color: Colors.black),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  height: 170,
                  width: 164,
                  child: Stack(children: [
                    Container(
                      child: Image.asset('assets/image/topprogram.png'),
                    ),
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(top: 115),
                      child: Column(
                        children: [
                          Text(
                            'Activity',
                            style: FontStyle.f1
                                .copyWith(fontSize: 14, color: Colors.white),
                          ),
                          Text(
                            '120 Excercises',
                            style: FontStyle.f4
                                .copyWith(fontSize: 14, color: Colors.white),
                          )
                        ],
                      ),
                    )
                  ]),
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(10)),
                ),
                Column(
                  children: [
                    Container(
                        padding: EdgeInsets.only(left: 13, top: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Mindfulness',
                              style: FontStyle.f2
                                  .copyWith(fontSize: 14, color: Colors.white),
                            ),
                            Text(
                              '35 Excercises',
                              style: FontStyle.f4
                                  .copyWith(fontSize: 14, color: Colors.white),
                            ),
                          ],
                        ),
                        height: 75,
                        width: 164,
                        decoration: BoxDecoration(
                            color: Color(0xff04004F),
                            borderRadius: BorderRadius.circular(10))),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                        padding: EdgeInsets.only(left: 13, top: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Nutrition',
                              style: FontStyle.f2.copyWith(
                                  fontSize: 14, color: Color(0xff04004F)),
                            ),
                            Text(
                              '300 Recipes',
                              style: FontStyle.f4.copyWith(
                                  fontSize: 14, color: Color(0xff04004F)),
                            ),
                          ],
                        ),
                        height: 75,
                        width: 164,
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black.withOpacity(0.2),
                                  blurRadius: 5,
                                  offset: Offset(0, 3))
                            ],
                            color: Color(0xffF7F8F8),
                            borderRadius: BorderRadius.circular(10))),
                  ],
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 15, bottom: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Assessments',
                    style: FontStyle.f4
                        .copyWith(color: Colors.black, fontSize: 17),
                  ),
                  RichText(
                      text: TextSpan(
                          text: 'See more',
                          style: FontStyle.f1.copyWith(
                              color: Color(0xff9B99B9), fontSize: 12)))
                ],
              ),
            ),
            SecondActivities(
              firstText: 'Mental Health',
              secondText: '3 Minutes',
              image:
                  Container(child: Image.asset('assets/image/2ndactivity.png')),
            ),
            SizedBox(
              height: 15,
            ),
            SecondActivities(
              firstText: 'Empathy',
              secondText: '5 Minutes',
              image: Container(
                  child: Image.asset('assets/image/doubleworkout.png')),
            ),
          ],
        ),
      ),
    ));
  }
}
