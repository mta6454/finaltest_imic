import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class ThirdScreen extends StatelessWidget {
  const ThirdScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
          child: Column(
        children: [
          Container(
              color: Colors.yellow,
              width: double.infinity,
              child: Image.asset(
                'assets/image/insight.png',
                width: double.infinity,
                fit: BoxFit.fill,
              )),
          SizedBox(
            height: 80,
          )
        ],
      )),
    );
  }
}
