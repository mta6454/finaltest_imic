import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'package:flutter_finaltest/screen/first_screen.dart';
import 'package:flutter_finaltest/screen/forth_screen.dart';
import 'package:flutter_finaltest/screen/profilescreen.dart';
import 'package:flutter_finaltest/screen/third_screen.dart';
import 'package:flutter_finaltest/screen/widget/dock_button.dart';
import 'package:flutter_finaltest/screen/second_screen.dart';
import 'package:flutter_svg/svg.dart';

class MainScreen extends StatefulWidget {
  MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  List<ButtonNavigator> listButton = [
    ButtonNavigator(
      title: 'home',
      image: Container(
        child: SvgPicture.asset('assets/svg/homeicon.svg'),
      ),
      imageenabled: Container(
        child: SvgPicture.asset('assets/svg/homeiconenabled.svg'),
      ),
    ),
    ButtonNavigator(
      title: 'coach',
      image: Container(
        child: SvgPicture.asset('assets/svg/coach.svg'),
      ),
      imageenabled: Container(
        child: SvgPicture.asset('assets/svg/coachenabled.svg'),
      ),
    ),
    ButtonNavigator(
      title: 'insight',
      image: Container(
        child: SvgPicture.asset('assets/svg/insight.svg'),
      ),
      imageenabled: Container(
        child: SvgPicture.asset('assets/svg/insightenabled.svg'),
      ),
    ),
    ButtonNavigator(
      title: 'explore',
      image: Container(
        child: SvgPicture.asset('assets/svg/explore.svg'),
      ),
      imageenabled: Container(
        child: SvgPicture.asset('assets/svg/exploreenabled.svg'),
      ),
    ),
    ButtonNavigator(
      title: 'profile',
      image: Container(
        child: SvgPicture.asset('assets/svg/profile.svg'),
      ),
      imageenabled: Container(
        child: SvgPicture.asset('assets/svg/profileenabled.svg'),
      ),
    ),
  ];
  @override
  Widget build(BuildContext context) {
    // bool isActive;

    return Scaffold(
      body: Container(
        child: Stack(
          children: [
            detected == 'home'
                ? FirstScreen()
                : detected == 'coach'
                    ? SecondScreen()
                    : detected == 'insight'
                        ? ThirdScreen()
                        : detected == 'explore'
                            ? FourthScreen()
                            : ProfileScreen(),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 80,
                decoration: BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                      color: Colors.black.withOpacity(0.25),
                      blurRadius: 5,
                      offset: Offset(0, -3))
                ]),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: buttonWidget()),
              ),
            ),
          ],
        ),
      ),
    );
  }

  String detected = 'home';
  List<Widget> buttonWidget() {
    return listButton.map((e) {
      return DockButton(
        title: e.title,
        image: e.image,
        imageEnabled: e.imageenabled,
        isActive: detected != e.title,
        onTap: () {
          setState(() {
            detected = e.title!;
          });
        },
      );
    }).toList();
  }
}

class ButtonNavigator {
  final String? title;
  final Widget? image;
  final Widget? imageenabled;
  final Widget? screen;
  ButtonNavigator({this.image, this.imageenabled, this.title, this.screen});
}
