import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_finaltest/font.dart';

class RecommendedWidget extends StatelessWidget {
  RecommendedWidget({Key? key, this.firstText, this.secondText, this.image})
      : super(key: key);
  String? firstText;
  String? secondText;
  Widget? image;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 18, vertical: 20),
      height: 132,
      decoration: BoxDecoration(
          color: Color(0xffF7F8F8),
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                color: Colors.black.withOpacity(0.25),
                offset: Offset(0, 5),
                blurRadius: 5)
          ]),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              firstText!,
              style:
                  FontStyle.f2.copyWith(fontSize: 14, color: Color(0xff04004F)),
            ),
            Text(
              secondText!,
              style:
                  FontStyle.f1.copyWith(fontSize: 12, color: Color(0xff9B99B9)),
            ),
            SizedBox(
              height: 17,
            ),
            Container(
              child: Center(
                child: Text(
                  'View more',
                  style:
                      FontStyle.f1.copyWith(fontSize: 12, color: Colors.white),
                ),
              ),
              height: 35,
              width: 94,
              decoration: BoxDecoration(
                  color: Color(0xff00DDA3),
                  borderRadius: BorderRadius.circular(10)),
            )
          ],
        ),
        image!
      ]),
    );
  }
}
