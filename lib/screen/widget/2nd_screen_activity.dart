import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_finaltest/font.dart';

class SecondActivities extends StatefulWidget {
  SecondActivities({Key? key, this.firstText, this.image, this.secondText})
      : super(key: key);
  Widget? image;
  String? firstText;
  String? secondText;

  @override
  State<SecondActivities> createState() => _SecondActivitiesState();
}

class _SecondActivitiesState extends State<SecondActivities> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 15, right: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              widget.image!,
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.firstText!,
                      style: FontStyle.f2
                          .copyWith(color: Colors.black, fontSize: 12),
                    ),
                    Text(
                      widget.secondText!,
                      style: FontStyle.f1
                          .copyWith(color: Color(0xff9B99B9), fontSize: 10),
                    )
                  ],
                ),
              )
            ],
          ),
          Container(
            height: 35,
            width: 62,
            child: Center(
                child: Text(
              'Assess',
              style: FontStyle.f2.copyWith(color: Colors.white, fontSize: 10),
            )),
            decoration: BoxDecoration(
                color: Color(0xff00DDA3),
                borderRadius: BorderRadius.circular(10)),
          )
        ],
      ),
      decoration: BoxDecoration(
          color: Color(0xffF7F8F8),
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                color: Colors.black.withOpacity(0.25),
                blurRadius: 5,
                offset: Offset(0, 3))
          ]),
      height: 80,
    );
  }
}
