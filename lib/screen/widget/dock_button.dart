import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_finaltest/font.dart';
import 'package:flutter_svg/svg.dart';

class DockButton extends StatefulWidget {
  DockButton({
    Key? key,
    this.image,
    this.title,
    this.imageEnabled,
    this.onTap,
    this.isActive,
  }) : super(key: key);
  final bool? isActive;
  final Widget? image;
  final Widget? imageEnabled;
  final String? title;
  final GestureTapCallback? onTap;

  @override
  State<DockButton> createState() => _DockButtonState();
}

class _DockButtonState extends State<DockButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 75,
      child: InkWell(
          onTap: widget.onTap,
          child: widget.isActive == false
              ? Column(
                  children: [
                    Container(
                      height: 5,
                      width: 55,
                      decoration: BoxDecoration(
                          color: Color(0xff00DDA3),
                          borderRadius: BorderRadius.vertical(
                              bottom: Radius.circular(10))),
                    ),
                    SizedBox(height: 7),
                    widget.imageEnabled!,
                    SizedBox(height: 7),
                    Text(
                      widget.title!,
                      style: FontStyle.f4
                          .copyWith(color: Color(0xff00DDA3), fontSize: 10),
                    )
                  ],
                )
              : Column(
                  children: [
                    SizedBox(
                      height: 12,
                    ),
                    widget.image!,
                    SizedBox(
                      height: 7,
                    ),
                    Container(
                      child: Text(
                        widget.title!,
                        style: FontStyle.f1
                            .copyWith(color: Color(0xff04004F), fontSize: 10),
                      ),
                    )
                  ],
                )),
    );
  }
}
