import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class ActivityButton extends StatelessWidget {
  ActivityButton({Key? key, this.firstText, this.secondText, this.image})
      : super(key: key);
  Widget? firstText;
  Widget? secondText;
  Widget? image;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 12),
      height: 72,
      width: 164,
      decoration: BoxDecoration(
        color: Color(0xff04004F),
        borderRadius: BorderRadius.circular(10),
      ),
      child: InkWell(
        onTap: () {},
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              margin: EdgeInsets.only(top: 18),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [firstText!, secondText!],
              ),
            ),
            image!
          ],
        ),
      ),
    );
  }
}
