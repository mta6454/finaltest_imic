import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_finaltest/font.dart';

class FourthActivities extends StatefulWidget {
  FourthActivities({Key? key, this.firstText, this.secondText, this.image})
      : super(key: key);
  Widget? image;
  String? firstText;
  String? secondText;

  @override
  State<FourthActivities> createState() => _FourthActivitiesState();
}

class _FourthActivitiesState extends State<FourthActivities> {
  bool isActive = true;

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 80,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  blurRadius: 3,
                  offset: Offset(0, 3))
            ]),
        padding: EdgeInsets.only(left: 15, right: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(children: [
              widget.image!,
              Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.firstText!,
                        style: FontStyle.f2
                            .copyWith(color: Colors.black, fontSize: 12),
                      ),
                      Text(
                        widget.secondText!,
                        style: FontStyle.f1
                            .copyWith(color: Color(0xff9B99B9), fontSize: 10),
                      )
                    ],
                  )),
            ]),
            Container(
              height: 22,
              width: 22,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(45),
                  color: Color(0xff0FD8AB)),
              child: InkWell(
                onTap: () {},
                child: Icon(
                  Icons.arrow_forward_ios_rounded,
                  color: Colors.white,
                  size: 12,
                ),
              ),
            )
          ],
        ));
  }
}
