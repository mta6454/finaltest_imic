import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_finaltest/font.dart';

class UpcomingAcivities extends StatefulWidget {
  UpcomingAcivities({Key? key, this.firstText, this.secondText, this.image})
      : super(key: key);
  Widget? image;
  String? firstText;
  String? secondText;

  @override
  State<UpcomingAcivities> createState() => _UpcomingAcivitiesState();
}

class _UpcomingAcivitiesState extends State<UpcomingAcivities> {
  bool isActive = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 15, right: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              widget.image!,
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.firstText!,
                      style: FontStyle.f2
                          .copyWith(color: Colors.black, fontSize: 12),
                    ),
                    Text(
                      widget.secondText!,
                      style: FontStyle.f1
                          .copyWith(color: Color(0xff9B99B9), fontSize: 10),
                    )
                  ],
                ),
              )
            ],
          ),
          AnimatedContainer(
            duration: Duration(milliseconds: 100),
            height: 24,
            width: 44,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                color:
                    isActive == true ? Color(0xff00DDA3) : Color(0xff04004F)),
            child: InkWell(
              child: Row(
                children: [
                  Container(
                    margin: isActive == false
                        ? EdgeInsets.only(left: 5)
                        : EdgeInsets.only(left: 25),
                    height: 14,
                    width: 14,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(45)),
                  )
                ],
              ),
              onTap: (() {
                setState(() {
                  isActive = !isActive;
                });
              }),
            ),
          )
        ],
      ),
      decoration: BoxDecoration(
          color: Color(0xffF7F8F8),
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                color: Colors.black.withOpacity(0.25),
                blurRadius: 5,
                offset: Offset(0, 3))
          ]),
      height: 80,
    );
  }
}
