import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_finaltest/font.dart';
import 'package:flutter_finaltest/screen/widget/activity_button.dart';
import 'package:flutter_finaltest/screen/widget/recommended.dart';
import 'package:flutter_finaltest/screen/widget/upcomingactivities.dart';

class FirstScreen extends StatelessWidget {
  FirstScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            children: [
              Container(
                // color: Colors.yellow,
                margin: EdgeInsets.only(top: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Container(
                          padding: EdgeInsets.only(right: 10),
                          height: 48,
                          width: 48,
                          child: Image.asset('assets/image/Avatar.png'),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Welcome Back Arash',
                              style: FontStyle.f3.copyWith(
                                  color: Color(0xff00DDA3), fontSize: 15),
                            ),
                            Text(
                              'how are you feeling today?',
                              style: FontStyle.f1.copyWith(
                                  color: Color(0xff04004F), fontSize: 15),
                            )
                          ],
                        ),
                      ],
                    ),
                    Container(
                        child: InkWell(
                          borderRadius: BorderRadius.circular(8),
                          onTap: () {},
                          child: Image.asset('assets/image/notiicon.png'),
                        ),
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                            color: Color(0xffF7F8F8),
                            borderRadius: BorderRadius.circular(8))),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 11),
                height: 50,
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black.withOpacity(0.25),
                          blurRadius: 3,
                          offset: Offset(0, 3))
                    ],
                    borderRadius: BorderRadius.circular(10),
                    color: Color(0xffF7F8F8)),
                child: TextField(
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Search...',
                      prefixIcon: Icon(
                        Icons.search,
                        color: Color(0xff04004F),
                      )),
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(top: 22, bottom: 15),
                child: Text(
                  "Today's Plan",
                  style: FontStyle.f4
                      .copyWith(color: Color(0xff04004F), fontSize: 17),
                ),
              ),
              Wrap(
                spacing: 15,
                runSpacing: 15,
                children: [
                  ActivityButton(
                    image: Container(
                      child: Image.asset('assets/image/1stactivity.png'),
                    ),
                    secondText: Text(
                      '30 Minutes',
                      style: FontStyle.f1
                          .copyWith(color: Colors.white, fontSize: 14),
                    ),
                    firstText: Text(
                      'Activity',
                      style: FontStyle.f4
                          .copyWith(color: Colors.white, fontSize: 14),
                    ),
                  ),
                  ActivityButton(
                    image: Container(
                      child: Image.asset('assets/image/2ndactivity.png'),
                    ),
                    secondText: Text(
                      '15 Minutes',
                      style: FontStyle.f1
                          .copyWith(color: Colors.white, fontSize: 14),
                    ),
                    firstText: Text(
                      'Meditate',
                      style: FontStyle.f4
                          .copyWith(color: Colors.white, fontSize: 14),
                    ),
                  ),
                  ActivityButton(
                    image: Container(
                      child: Image.asset('assets/image/3rdactivity.png'),
                    ),
                    secondText: Text(
                      '2 recipes',
                      style: FontStyle.f1
                          .copyWith(color: Colors.white, fontSize: 14),
                    ),
                    firstText: Text(
                      'Food',
                      style: FontStyle.f4
                          .copyWith(color: Colors.white, fontSize: 14),
                    ),
                  ),
                  Container(
                    child: Center(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Let's Go",
                          style: FontStyle.f4
                              .copyWith(color: Colors.white, fontSize: 18),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 4),
                          height: 4,
                          width: 61,
                          decoration: BoxDecoration(
                              color: Color(0xff04004F),
                              borderRadius: BorderRadius.circular(10)),
                        )
                      ],
                    )),
                    height: 72,
                    width: 164,
                    decoration: BoxDecoration(
                        color: Color(0xff00DDA3),
                        borderRadius: BorderRadius.circular(10)),
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 15, bottom: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Upcoming Activities',
                      style: FontStyle.f4
                          .copyWith(color: Colors.black, fontSize: 17),
                    ),
                    RichText(
                        text: TextSpan(
                            text: 'See more',
                            style: FontStyle.f1.copyWith(
                                color: Color(0xff9B99B9), fontSize: 12)))
                  ],
                ),
              ),
              UpcomingAcivities(
                image: Container(
                  child: Image.asset('assets/image/workout.png'),
                ),
                firstText: 'Fullbody Workout',
                secondText: 'Today, 03:00 pm',
              ),
              SizedBox(
                height: 15,
              ),
              UpcomingAcivities(
                image: Container(
                  child: Image.asset('assets/image/upperworkout.png'),
                ),
                firstText: 'Upperbody Workout',
                secondText: 'August 15, 02:00 pm',
              ),
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(top: 22, bottom: 15),
                child: Text(
                  'Recommended for you',
                  style:
                      FontStyle.f4.copyWith(color: Colors.black, fontSize: 17),
                ),
              ),
              Wrap(
                runSpacing: 12,
                children: [
                  RecommendedWidget(
                    firstText: 'Fullbody Workout',
                    secondText: '11 Exercises | 32mins',
                    image: Container(
                      child: Image.asset('assets/image/nhayday.png'),
                    ),
                  ),
                  RecommendedWidget(
                    firstText: 'Lowerbody Workout',
                    secondText: '12 Exercises | 40mins',
                    image: Container(
                      child: Image.asset('assets/image/lowerbodyworkout.png'),
                    ),
                  ),
                  RecommendedWidget(
                    firstText: 'AB Workout',
                    secondText: '14 Exercises | 20mins',
                    image: Container(
                      child: Image.asset('assets/image/abworkout.png'),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 100,
              )
            ],
          ),
        ),
      ),
    );
  }
}
